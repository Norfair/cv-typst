{
  description = "cv";

  inputs = {
    # We need a newer typst
    # Set this back to a realease in 2024-05
    nixpkgs.url = "github:NixOS/nixpkgs?ref=nixpkgs-unstable";
    pre-commit-hooks.url = "github:cachix/pre-commit-hooks.nix";
    typst-nix.url = "github:NorfairKing/typst.nix";
    typst-nix.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs =
    { self
    , nixpkgs
    , pre-commit-hooks
    , typst-nix
    }:
    let
      system = "x86_64-linux";
      withPageCheck = maxPages: drv: drv.overrideAttrs (old: {
        buildCommand = ''
          ${old.buildCommand or ""}

          set -e
          pages="$(${pkgs.pdftk}/bin/pdftk $out dump_data | grep NumberOfPages | sed 's/[^0-9]*//')"
  
          if [ "$pages" -gt "${builtins.toString maxPages}" ]
          then
            echo "PDF was too long: $pages pages instead of ${builtins.toString maxPages} or fewer"
            exit 1
          fi
        '';
      });
      overlay = final: prev:
        {
          cvRelease = final.stdenv.mkDerivation {
            name = "cvs";
            dontUnpack = true;
            buildCommand = ''
              mkdir $out

              ln -s ${final.cvs.cv} $out/cv.pdf
              ln -s ${final.cvs.ic} $out/ic.pdf
              ln -s ${final.cvs.tl} $out/tl.pdf
              ln -s ${final.cvs.haskell} $out/haskell.pdf
              ln -s ${final.cvs.fail} $out/fail.pdf
            '';
          };
          cvs = {
            cv = withPageCheck 1 (final.makeTypstDocument {
              name = "cv.pdf";
              main = "cv.typ";
              src = ./src;
            });
            ic = withPageCheck 2 (final.makeTypstDocument {
              name = "ic.pdf";
              main = "ic.typ";
              src = ./src;
            });
            tl = withPageCheck 2 (final.makeTypstDocument {
              name = "tl.pdf";
              main = "tl.typ";
              src = ./src;
            });
            haskell = withPageCheck 2 (final.makeTypstDocument {
              name = "haskell.pdf";
              main = "haskell.typ";
              src = ./src;
            });
            fail = final.makeTypstDocument {
              name = "fail.pdf";
              main = "fail.typ";
              src = ./src;
            };
          };
        };
      pkgsFor = nixpkgs: import nixpkgs {
        inherit system;
        config.allowUnfree = true;
        overlays = [
          typst-nix.overlays.${system}
          overlay
        ];
      };
      pkgs = pkgsFor nixpkgs;
    in
    {
      overlays.${system} = overlay;
      packages.${system} = {
        default = pkgs.cvRelease;
      } // pkgs.cvs;
      checks.${system} = {
        package = self.packages.${system}.default;
        shell = self.devShells.${system}.default;
        pre-commit = pre-commit-hooks.lib.${ system}.run {
          src = ./.;
          hooks = {
            nixpkgs-fmt.enable = true;
            typstfmt.enable = true;
            yamlfmt = {
              enable = true;
              name = "yamlfmt";
              entry = "${pkgs.yamlfmt}/bin/yamlfmt";
              types = [ "file" ];
              files = "\\.(yaml)$";
            };
          };
        };
      };
      devShells.${system}.default = pkgs.mkShell {
        name = "cv-shell";
        buildInputs = (with pkgs; [
          typst
        ]) ++ (with pre-commit-hooks.packages.${system};
          [
            nixpkgs-fmt
            typstfmt
          ]);
        shellHook = self.checks.${system}.pre-commit.shellHook;
      };
    };
}
