#import "template.typ": *

#show: resume.with(author: (
  name: "Tom Sydney Kerckhove",
  email: "syd@cs-syd.eu",
  github: "NorfairKing",
  linkedin: "tomsydneykerckhove",
  website: "cs-syd.eu",
  positions: ("Technical Leader with over a decade of experience",),
), date: datetime.today().display())

#align(
  center,
)[
  #text(
    size: 9pt,
  )[
    This is a generic CV and as such it is probably not the one you are looking for.
    Please find more specific CVs at #link("https://cs-syd.eu/cv", raw("https://cs-syd.eu/cv")).
  ]
]

#resume_section("Experience")

#let experience = (yaml("data/experience.yaml"))

#text(size: 15pt, weight: "bold", [
  12+ Years of Technical experience: Haskell, Nix, Rust \
  6+ Years of Engineering Management experience
])

#for e in experience.filter(e => e.relevance.cv) [
  #work_experience_item_header(e.position, e.location, e.company, [#(e.start_date) - #(e.end_date)])

  #resume_item[
    #if type(e.highlights.at(0, default: "")) == str [
      #list(tight: true, ..e.highlights)
    ] else [
      #list(..e.highlights.filter(i=> i.relevance.cv).map(i=>[
        #text(weight: "bold", i.title)
        #list(tight: true, ..i.highlights, indent: 10pt)
      ]), tight: true, indent: 5pt)
    ]
  ]
]

#include "education.typ"
