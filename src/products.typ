#import "template.typ": *

#resume_section("Products")

#let products = yaml("data/products.yaml")

#for p in products [
  #personal_project_item_header(
    [#(p.name): #(p.description)],
    link("https://" + p.website, raw(p.website)),
    p.stack,
    "",
  )

  #for h in p.highlights [
    - #resume_item(h)
  ]
]
