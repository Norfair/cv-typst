== Languages

#let languages = yaml("data/languages.yaml")

// TODO get rid of borders
#table(
  columns: (auto, auto),
  align: (x, y) => (left, left).at(x),
  stroke: none,
  row-gutter: 5pt,
  column-gutter: 5pt,
  inset: (x: 0pt, y: 0pt),
  ..(languages.map(l => (l.language, l.fluency))).flatten(),
)
