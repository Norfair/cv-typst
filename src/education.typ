#import "template.typ": resume_section, education_item

#resume_section("Education")

#let education = (yaml("data/education.yaml"))

#for e in education [
  #education_item(
    [#(e.study_type) at #(e.institution)],
    e.note,
    [#(e.start_date) - #(e.end_date)],
    [],
  )
]
