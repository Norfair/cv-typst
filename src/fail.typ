#import "template.typ": *

#show: resume.with(author: (
  name: "Tom Sydney Kerckhove",
  email: "syd@cs-syd.eu",
  github: "NorfairKing",
  linkedin: "tomsydneykerckhove",
  website: "cs-syd.eu",
  positions: ("Tom Sydney Kerckhove",),
), date: datetime.today().display())

#align(center, [= CV of failures])

#v(10pt)

This CV lists failures I've experienced in my career.

A list of failures might seem depressing, but consider:

- It is important to be transparent about the fact that failures occur, a lot. A
  CV might be impressive, but it does not show the failed attempts, it only shows
  the successes.
- This lists gives a purpose to failures. Failing is not fun, but being able to
  put each failure on a CV of failures gives a silver lining to each failure.

Please keep in mind that this document only mentions the failures. It does not
mention any circumstances that might explain how the failure is not necessarily
a reflection of my skill, character or work ethic.

#v(10pt)

#let failures = yaml("data/failures.yaml")

#list(
  ..failures.map(
    f=> [#(f.date): #text(f.institution, weight: "bold"): #(f.description)],
  ),
)

